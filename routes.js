'use strict';

module.exports = function(app) {

  var nodes = global.nodes;

  // app.use('/api/controllers', require('./api/controllers'));
  // app.use('/api/groups', require('./api/group'));
  app.use('/api/nodes', require('./api/node'));
  app.use('/api/sensors', require('./api/sensor'));
  app.use('/api/dimmers', require('./api/dimmer'));
  app.use('/api/switches', require('./api/switch'));

  app.route('/:url(api|assets|node_modules)/*')
   .get(function(req, res) {
     res.send("This page doesn't exists.", 404);
     // res.sendfile(app.get('appPath') + '/index.html');
   });

  app.route('/*')
   .get(function(req, res) {
     // res.send('Number of nodes: ' + (nodes.length-1));
     var infos = {
         infos: 'Domo-server v.1.0',
         paths: {
             nodes: 'api\/nodes\/',
             dimmers: 'api\/dimmers\/',
             sensors: 'api\/sensors\/',
             switches: 'api\/switches\/'
         }
     }
     res.json(infos);
   });
};
