'use strict';



/*
 * List all sensors.
 * JSON payload example: {sensors: Array}
 */
exports.index = function(req, res) {

  res.json({sensors: []});
}
/*
 * Get infos on a specific sensor.
 * JSON payload example: {sonsor: Object}
 */
exports.show = function(req, res) {

  res.json({sensor: {}});
}
