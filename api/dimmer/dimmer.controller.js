'use strict';

var nodes = global.nodes;

/*
 * List all switches.
 * JSON payload example: {switches: Array}
 */
exports.index = function(req, res) {
    var dimmers = [];
    nodes.forEach(function(node, index) {
        if(node.classes[38] !== undefined) {
            dimmers.push({id: index, node: node});
        }
    });
    res.json(dimmers);
}
/*
 * Return info on selected dimmer.
 */
exports.show = function(req, res) {
    var dimmer = nodes[req.params.id];
    if(dimmer !== undefined && dimmer.classes[38] !== undefined)
      res.json(dimmer);
    else
      res.status(400).send('Wrong ID');
}

/*
 * Change dimmer #id level.
 * JSON payload example: {level: Number}
 */
exports.update = function(req, res) {
  const id = req.params.id;
  const oldLevel = nodes[id].classes[38][0].value;
  const level = req.body.level;

  // Dimmer = class 38
  zwave.setValue(id, 38, 1, 0, level);

  res.json({ before: oldLevel, after: nodes[id].classes[38][0].value, result: 'success' });
}
