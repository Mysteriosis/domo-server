'use strict';

// LIBRARIES
var OZW = require('openzwave-shared');
var express = require('express');
var bodyParser = require('body-parser');

// VARIABLES
var DEVICE_NAME = '/dev/ttyACM0';
var APP_PORT = 3000;

var nodes = [];
global.nodes = nodes;

var zwave = new OZW({
    ConsoleOutput: false,
    Logging: true,
    PollInterval: 500,
    DriverMaxAttempts: 3
});
global.zwave = zwave;

// ZWAVE PART

/*
 * The OpenZWave driver has initialised and scanning has started.
 * Returns a unique homeid which identifies this particular ZWave network.
 */
zwave.on('driver ready', function(homeid) {
    console.log('scanning homeid=0x%s...', homeid.toString(16));

    app.listen(APP_PORT, function() {
      console.log("Domo Server listening on port " + APP_PORT);
    });
});

/*
 * The OpenZWave driver failed to initialise.
 */
zwave.on('driver failed', function() {
    console.log('Failed to start driver. Are you sudo ?');
    zwave.disconnect();
    process.exit();
});

/*
 * A new node has been found on the network.
 * At this point you can allocate resources to hold information about this node.
 */
zwave.on('node added', function(nodeid) {
    nodes[nodeid] = {
        id: '',
        manufacturer: '',
        manufacturerid: '',
        product: '',
        producttype: '',
        productid: '',
        type: '',
        name: '',
        loc: '',
        classes: {},
        ready: false
    };
});

/*
 * This corresponds to OpenZWave's NodeQueriesComplete notification.
 * The node is now ready for operation, and information about the
 * node is available in the nodeinfo object
 */
zwave.on('node ready', function(nodeid, nodeinfo) {
    nodes[nodeid]['id'] = nodeid;
    nodes[nodeid]['manufacturer'] = nodeinfo.manufacturer;
    nodes[nodeid]['manufacturerid'] = nodeinfo.manufacturerid;
    nodes[nodeid]['product'] = nodeinfo.product;
    nodes[nodeid]['producttype'] = nodeinfo.producttype;
    nodes[nodeid]['productid'] = nodeinfo.productid;
    nodes[nodeid]['type'] = nodeinfo.type;
    nodes[nodeid]['name'] = nodeinfo.name;
    nodes[nodeid]['loc'] = nodeinfo.loc;
    nodes[nodeid]['ready'] = true;

    console.log('node%d: %s, %s', nodeid,
        nodeinfo.manufacturer ? nodeinfo.manufacturer : 'id=' + nodeinfo.manufacturerid,
        nodeinfo.product ? nodeinfo.product : 'product=' + nodeinfo.productid + ', type=' + nodeinfo.producttype);

    console.log('node%d: name="%s", type="%s", location="%s"', nodeid,
        nodeinfo.name,
        nodeinfo.type,
        nodeinfo.loc);

    var comclass;
    for (comclass in nodes[nodeid]['classes']) {
        switch (comclass) {
            case 0x25: // COMMAND_CLASS_SWITCH_BINARY
            case 0x26: // COMMAND_CLASS_SWITCH_MULTILEVEL
                zwave.enablePoll(nodeid, comclass);
                break;
        }

        var values = nodes[nodeid]['classes'][comclass];

        console.log('node%d: class %d', nodeid, comclass);

        var idx;
        for (idx in values) {
            console.log('node%d: %s = %s', nodeid,
                values[idx]['label'],
                values[idx]['value']);
        }
    }
});

/*
 * A new node has been found on the network.
 * At this point you can allocate resources to hold information
 * about this node.
 */
zwave.on('value added', function(nodeid, comclass, value) {
    if (!nodes[nodeid]['classes'][comclass]) {
        nodes[nodeid]['classes'][comclass] = {};
    }
    nodes[nodeid]['classes'][comclass][value.index] = value;
});

/*
 * A value has changed.
 * Use this to keep track of value state across the network.
 * When values are first discovered, the module enables polling on
 * those values so that we will receive change messages.
 * Prior to the 'node ready' event, there may be 'value changed' events
 * even when no values were actually changed.
 */
zwave.on('value changed', function(nodeid, comclass, value) {
    if (nodes[nodeid]['ready']) {
        console.log('node%d: changed: %d:%s:%s->%s', nodeid, comclass,
                value['label'],
                nodes[nodeid]['classes'][comclass][value.index]['value'],
                value['value']);
    }
    nodes[nodeid]['classes'][comclass][value.index] = value;
});

/*
 * A value has been removed.
 * Your program should then remove any references to that ValueID.
 */
zwave.on('value removed', function(nodeid, comclass, index) {
    if (nodes[nodeid]['classes'][comclass] && nodes[nodeid]['classes'][comclass][index]) {
        delete nodes[nodeid]['classes'][comclass][index];
    }
});

zwave.connect(DEVICE_NAME);

// EXPRESS PART
var app = express();

app.use(bodyParser.json());

require('./routes')(app);
// app starts when z-wave network is ready => in zwave.on('driver ready')

// OTHER
process.on('SIGINT', function() {
    console.log(' Disconnecting...');
    zwave.disconnect();
    process.exit();
});
