'use strict';

var nodes = global.nodes;
var zwave = global.zwave;

/*
 * List all switches.
 * JSON payload example: {switches: Array}
 */
exports.index = function(req, res) {
    var switches = [];
    nodes.forEach(function(node, index) {
        if(node.classes[37] !== undefined) {
            switches.push({id: index, node: node});
        }
    });
    res.json(switches);
}
/*
 * Return info on selected switch.
 */
exports.show = function(req, res) {
    var switches = nodes[req.params.id];
    res.json(switches);
}

/*
 * Toggle switch #id.
 * JSON payload example: {value: "on | off"}
 */
exports.update = function(req, res) {
  var id = req.params.id;
  var value = req.body.value;

  switch(value) {
    case "on":
      //zwave.setNodeOn(id);
      zwave.setValue(id, 37, 1, 0, true); // node 3: turn on
      break;
    case "off":
      //zwave.setNodeOff(id);
      zwave.setValue(id, 37, 1, 0, false); // node 3: turn off
      break;
  }

  res.json({result: 'success', message: 'Node turned ' + value});
}
