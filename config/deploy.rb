require 'json'

class SSHKit::Sudo::InteractionHandler
  use_same_password!
end

# config valid only for current version of Capistrano
lock "3.8.2"

set :application, "domo-server"
set :repo_url, "git@github.com:Mysteriosis/domo-server.git"
set :branch, 'master'
set :pty, true

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "~/domo-server"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"
# append :linked_files, ["config/secrets.json"]

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  desc "Stop domo-server"
  task :started do
    on roles(:app) do
      sudo "forever stopall"
    end
  end

  desc "Install node modules non-globally"
  task :updated do
    on roles(:app) do
      execute "echo #{release_path}"
      within "#{release_path}" do
        sudo "npm update"
      end
    end
  end

  desc "Start domo-server"
  task :finished do
    on roles(:app) do
      within "#{release_path}" do
        sudo "forever start -c \"npm start\" ./"
      end
    end
  end

end
