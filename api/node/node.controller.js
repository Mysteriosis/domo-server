'use strict';

var nodes = global.nodes;

exports.index = function(req, res) {
    res.json(nodes.filter((e) => e !== null && e !== undefined).map((e) => {
      e.classes = Object.keys(e.classes).length + " classes available."
    }));
}

exports.show = function (req, res) {
  var node = nodes[req.params.id];
  res.json(node);
};
